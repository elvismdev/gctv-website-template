<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 */
?>

<footer class="dark-div">
  <?php if ( is_404() && is_active_sidebar( 'footer_404_sidebar' ) ) { ?>
    <div id="bottom">
      <div class="container">
        <div class="row">
         <?php dynamic_sidebar( 'footer_404_sidebar' ); ?>
       </div><!--/row-->
     </div><!--/container-->
   </div><!--/bottom-->
   <?php } elseif ( is_active_sidebar( 'footer_sidebar' ) ) { ?>
     <div id="bottom">
      <div class="container">
        <div class="row">
         <?php dynamic_sidebar( 'footer_sidebar' ); ?>
       </div><!--/row-->
     </div><!--/container-->
   </div><!--/bottom-->
   <?php } ?>
   <?php tm_display_ads('adv_foot');?>

   <div id="bottom-nav">
     <div class="container">
      <div class="row">
       <div class="copyright col-md-3 md-aligncenter"><?php echo ot_get_option('copyright',get_bloginfo('name').' - '.get_bloginfo('description')); ?></div>
       <nav class="col-md-9">
         <ul class="bottom-menu list-inline pull-right">
           <?php
           if(has_nav_menu( 'footer-navigation' )){
             wp_nav_menu(array(
              'theme_location'  => 'footer-navigation',
              'container' => false,
              'items_wrap' => '%3$s'
              ));
            }?>
          </ul>
        </nav>
      </div><!--/row-->
    </div><!--/container-->
  </div>

  <div id="footer-gc-properties-logos">
    <div class="container">
      <div class="row">
        <div class="col-xs-6 col-sm-3">
          <a target="_blank" href="http://www.grantcardone.com/"><img class="property-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/GrantCardone.png"></a>
        </div>
        <div class="col-xs-6 col-sm-3">
          <a target="_blank" href="http://cardoneondemand.com/"><img class="property-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/CardoneOnDemand.png"></a>
        </div>
        <div class="col-xs-6 col-sm-3">
          <a target="_blank" href="http://cardoneuniversity.com/"><img class="property-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/CardoneUniversity.png"></a>
        </div>
        <div class="col-xs-6 col-sm-3">
          <a target="_blank" href="http://cardoneacquisitions.com/"><img class="property-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/CardoneAcquisitions-logo.png"></a>
        </div>
      </div>
    </div>
  </div>
  
</footer>
<div class="wrap-overlay"></div>
</div><!--wrap-->

<?php if(ot_get_option('mobile_nav',1)){ ?>
  <div id="off-canvas">
    <div class="off-canvas-inner">
      <nav class="off-menu">
        <ul>
          <li class="canvas-close"><a href="#"><i class="fa fa-times"></i> <?php _e('Close','cactusthemes'); ?></a></li>
          <?php
          if(has_nav_menu( 'main-navigation' )){
            wp_nav_menu(array(
              'theme_location'  => 'main-navigation',
              'container' => false,
              'items_wrap' => '%3$s'
              ));
          }else{?>
            <li><a href="<?php echo home_url(); ?>/"><?php _e('Home','cactusthemes'); ?></a></li>
            <?php wp_list_pages('title_li=' ); ?>
            <?php } ?>
            <?php
            $user_show_info = ot_get_option('user_show_info');
            if ( is_user_logged_in() && $user_show_info =='1') {
              $current_user = wp_get_current_user();
              $link = get_edit_user_link( $current_user->ID );
              ?>
              <li class="menu-item current_us">
                <?php
                echo '<a class="account_cr" href="#">'.$current_user->user_login;
                echo get_avatar( $current_user->ID, '25' ).'</a>';
                ?>
                <ul class="sub-menu">
                  <li class="menu-item"><a href="<?php echo $link; ?>"><?php _e('Edit Profile','cactusthemes') ?></a></li>
                  <li class="menu-item"><a href="<?php echo wp_logout_url( get_permalink() ); ?>"><?php _e('Logout','cactusthemes') ?></a></li>
                </ul>
              </li>
              <?php }?>
                <?php //submit menu
                if(ot_get_option('user_submit',1)) {
                 $text_bt_submit = ot_get_option('text_bt_submit');
                 if($text_bt_submit==''){ $text_bt_submit = 'Submit Video';}
                 if(ot_get_option('only_user_submit',1)){
                  if(is_user_logged_in()){?>
                    <li class="menu-item"><a class="submit-video" href="#" data-toggle="modal" data-target="#submitModal"><?php _e($text_bt_submit,'cactusthemes'); ?></a></li>
                    <?php }
                  } else{
                   ?>
                   <li class="menu-item"><a class="submit-video" href="#" data-toggle="modal" data-target="#submitModal"><?php _e($text_bt_submit,'cactusthemes'); ?></a></li>
                   <?php

                 }
               } ?>
             </ul>
           </nav>
         </div>
       </div><!--/off-canvas-->
       <script>off_canvas_enable=1;</script>
       <?php }?>
       <?php if(ot_get_option('theme_layout',false)){ ?>
       </div><!--/boxed-container-->
       <?php }?>
       <div class="bg-ad">
         <div class="container">
           <div class="bg-ad-left">
             <?php tm_display_ads('ad_bg_left');?>
           </div>
           <div class="bg-ad-right">
             <?php tm_display_ads('ad_bg_right');?>
           </div>
         </div>
       </div>
     </div><!--/body-wrap-->
     <?php
     if(ot_get_option('user_submit',1)) {?>
       <div class="modal fade" id="submitModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 class="modal-title" id="myModalLabel"><?php _e('Submit Video'); ?></h4>
           </div>
           <div class="modal-body">
             <?php dynamic_sidebar( 'user_submit_sidebar' ); ?>
           </div>
         </div>
       </div>
     </div>
     <?php } ?>

     <a href="#top" id="gototop" class="notshow" title="Go to top"><i class="fa fa-angle-up"></i></a>

     <?php wp_footer(); ?>

     <script type="text/javascript">

      var addthis_config = {
       data_track_clickback: false
     }

   </script>

   <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-4793744-37', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
  </script>

  <script>
// var appInfo = {...}; // Should have siteId, articleId, etc.
// appInfo.actionButtons = [
// {
//   key: 'Do Something',
//   callback: function(commentInfo) {
//     console.log('Author of content is ' + commentInfo.authorId);
//     console.log('id of content is ' + commentInfo.contentId);
//   }
// },
// ];

// fyre.conv.load(globalInfo, [appInfo], function(app) {});
</script>

<!-- Sticky Loginout -->
<script type="text/javascript">
  jQuery( document ).ready(function( $ ){
    var $login_logout = $( '.loginout-area-wrapper' );
    $login_logout.sticky({
      topSpacing: 55,
      className: 'loginout-area-fixed'
    });
  });
</script>


<!-- Modal -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
        <img src="http://grantcardonetv.com/wp-content/uploads/login.jpg" />
      </div>
      <div class="modal-body" style="padding:0px;">
        <div class="padding">
          <?php wp_login_form(); ?>
        </div>
        <div class="susbcribe-link">
          <span>
            <a class="btn" href="<?php echo get_site_url(); ?>/subscribe">Create Your Free Account</a>
          </span>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body" style="padding:0px;">
        <div><img src="http://grantcardonetv.com/wp-content/uploads/bannersube22.jpg" /></div>
        <div class="padding">
          <?php echo do_shortcode("[gravityform id=\"1\" name=\"User Registration Form\" title=\"false\" description=\"false\"]"); ?>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>