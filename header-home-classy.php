<?php
global $header_query;
$header_bg = ot_get_option('header_home_bg');
?>
<style type="text/css">
	#classy-carousel{
		<?php if($header_bg['background-color']){ echo 'background-color:'.$header_bg['background-color'].';';} ?>
		<?php if($header_bg['background-attachment']){ echo 'background-attachment:'.$header_bg['background-attachment'].';';} ?>
		<?php if($header_bg['background-repeat']){
			echo 'background-repeat:'.$header_bg['background-repeat'].';';
			echo 'background-size: initial;';
		} ?>
		<?php if($header_bg['background-position']){ echo 'background-position:'.$header_bg['background-position'].';';} ?>
		<?php if($header_bg['background-image']){ echo 'background-image:url('.$header_bg['background-image'].');';} ?>
	}
	<?php if($header_bg['background-attachment']=='fixed'){ ?>
		@media(min-width:768px){
			#body-wrap{
				position:fixed;
				top:0;
				bottom:0;
				left:0;
				right:0;
			}
			.admin-bar #body-wrap{
				top:32px;
			}
		}
		@media(min-width:768px) and (max-width:782px){
			.admin-bar #body-wrap{
				top:46px;
			}
			.admin-bar #off-canvas{
				top:46px;
			}
		}
		.bg-ad {
			right: 14px;
		}
		<?php if(ot_get_option('theme_layout')!=1){ ?>
			#body-wrap{
				position:fixed;
				top:0;
				bottom:0;
				left:0;
				right:0;
			}
			.admin-bar #body-wrap{
				top:32px;
			}
			@media(max-width:782px){
				.admin-bar #body-wrap{
					top:46px;
				}
				.admin-bar #off-canvas{
					top:46px;
				}
			}
			<?php } 
		}?>

		html
		{
			margin-top:0px !important;
		}
	</style>


	<div style="height:38px !important;" class="hidden980"></div>
	<?php 
	$livelink = get_field('link_to_live_show', 'option'); 	
	$trailerlink = get_field( 'banner_video_link' ); 					
	$live = get_field('live_show_on_switch', 'option');
	$sitebanner = get_field('site_wide_banner', 'option');
	$showpage = get_field('showpage', 'option');
	$host = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	?>

	<a href="<?php echo $livelink;?>" target="_blank">
		<div class="offair <?php if($host == "$showpage") {echo 'hidden';} else {echo $sitebanner;} ?>" style="width:100% !impotant; ">
			<img src="/live-streaming-header.gif" border="0" />
		</div>
	</a>
	<div>

		<div class="row">
			<?php
			if ( class_exists( 'FragmentCache' ) ) {
				$rev_slider_cached = new FragmentCache( array( 'key' => 'home-rev-slider' ) );
				if ( !$rev_slider_cached->output() ) {
					putRevSlider('homepage');
					$rev_slider_cached->store();
				}
			} else {
				putRevSlider('homepage');
			} 
			?>

			<?php //if ( function_exists( 'soliloquy' ) ) { soliloquy( '20009' ); }	?>
		</div>
