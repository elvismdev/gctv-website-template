<?php
$condition = ot_get_option('header_home_condition','lastest');
$ids = ot_get_option('header_home_postids','');
$categories = ot_get_option('header_home_cat','');
$tags = ot_get_option('header_home_tag','');
$sort_by = ot_get_option('header_home_order','DESC');
$count = ot_get_option('header_home_number',12);
echo do_shortcode('[video_silder condition="'.$condition.'" order="'.$sort_by.'" count="'.$count.'" ids="'.$ids.'" categories="'.$categories.'" tags="'.$tags.'"]');
$page_layout = ot_get_option('page_layout');
?>
<style>
@media (min-width: 991px){
	<?php 
	if($page_layout =='right'){?>
	#slider .video_slider .video-with{ padding-left:25px; padding-right:0}
	#slider .video_slider{ padding-right:0}
	<?php
	}else{?>
	#slider .video_slider .video-with{ padding-right:25px;padding-left:0;}
	#slider .video_slider{ padding-left:0}
	<?php }
	?>
	#slider{ height:550px}
	#slider .is-carousel.simple-carousel.tm-video-slider .name.title h2{white-space: nowrap; overflow: hidden;text-overflow: ellipsis;
	}
	.row #content{ margin-top:-490px; background-color:#fff; padding-left:30px; padding-right:30px; padding-top:40px}
	.row #sidebar{ padding-left:0; padding-right:0}
}
@media (max-width: 991px){
	#slider .video_slider .video-with{ width:100%}
	.dark-div #slider  .is-carousel.simple-carousel.tm-video-slider .tt-content .link-overlay{ width:100%}
	#slider .video_slider .video-with .tt-content img{ width:100%}
}
</style>