<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
	<?php if(ot_get_option('favicon')):?>
		<link rel="shortcut icon" type="ico" href="<?php echo ot_get_option('favicon');?>">
	<?php endif;?>
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php if(ot_get_option('favicon')):?>
		<link rel="shortcut icon" type="ico" href="<?php echo ot_get_option('favicon');?>">
	<?php endif;?>
	<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<!--[if lte IE 9]>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie.css" />
<![endif]-->

<?php if(ot_get_option('retina_logo')):?>
	<style type="text/css" >
		@media only screen and (-webkit-min-device-pixel-ratio: 2),(min-resolution: 192dpi) {
			/* Retina Logo */
			.logo{background:url(<?php echo ot_get_option('retina_logo'); ?>) no-repeat center; display:inline-block !important; background-size:contain;}
			.logo img{ opacity:0; visibility:hidden}
			.logo *{display:inline-block}
		}
	</style>
<?php endif;?>

<?php wp_head();
$body_class = '';
if(ot_get_option('theme_layout',false)){
	$body_class .= 'boxed-mode';
}
?>

<link rel="stylesheet" type="text/css" href="/wp-content/themes/truemag/css/livecustomedits2-42.css" />
<?php if(is_ipad($version)) { echo '<link rel="stylesheet" type="text/css" href="/wp-content/themes/truemag/css/ipadfix.css" />'; }; ?>
<!-- <script type="text/javascript" src="/wp-content/themes/truemag/js/jquery-1.11.1.min.js"></script> -->
<!-- EXPANDABLE READ MORE BLOCKS -->
<script type="text/javascript">
	var $$ = jQuery.noConflict();

	$$(function(){
  var animspeed = 950; // animation speed in milliseconds
  
  var $$blockquote = $$('.bigtext');
  var height = $$blockquote.height();
  var mini = $$('.bigtext p').eq(0).height();
  
  $$blockquote.attr('data-fullheight',height+'px');
  $$blockquote.attr('data-miniheight',mini+'px');
  $$blockquote.css('height',mini+'px');
  

  $$('.expand').on('click', function(e){
  	$$text = $$(this).prev();

  	$$text.animate({
  		'height': $$text.attr('data-fullheight')
  	}, animspeed);
  	$$(this).next('.contract').removeClass('hide');
  	$$(this).addClass('hide');
  });
  
  $$('.contract').on('click', function(e){
  	$$text = $$(this).prev().prev();

  	$$text.animate({
  		'height': $$text.attr('data-miniheight')
  	}, animspeed);
  	$$(this).prev('.expand').removeClass('hide');
  	$$(this).addClass('hide');
  });
});
</script>
<!-- EXPANDABLE READ MORE BLOCKS END-->
<style>
	html
	{
		margin-top:0px !important;
	}
</style>


<meta property="og:image" content="<?php $thumb = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); echo $thumb; ?>" />

<!-- BEGIN Tynt Script -->
<script type="text/javascript">
	if(document.location.protocol=='http:'){
		var Tynt=Tynt||[];Tynt.push('bQjQLgFkKr5k8Aacwqm_6l');
		(function(){var s=document.createElement('script');s.async="async";s.type="text/javascript";s.src='http://tcr.tynt.com/ti.js';var h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();
	}
</script>
<!-- END Tynt Script -->

</head>

<body <?php body_class($body_class) ?>>

	<a name="top" style="height:0; position:absolute; top:0;" id="top-anchor"></a>

	<?php if(ot_get_option('loading_effect',2)==1||(ot_get_option('loading_effect',2)==2&&(is_front_page()||is_page_template('page-templates/front-page.php')))){ ?>
	<div id="pageloader">   
		<div class="loader-item">
			<span class="pulse"></span>
		</div>
	</div>
	<?php }?>

	<div id="body-wrap">
		<?php if(ot_get_option('theme_layout',false)){ ?>
		<div class="container boxed-container">
			<?php }?>
			<div id="wrap">
				<header class="">
					<?php
					if ( class_exists( 'FragmentCache' ) ) {
						$ubermenu_cached = new FragmentCache( array( 'key' => 'ubermenu-top-menu' ) );
						if ( !$ubermenu_cached->output() ) {
							ubermenu( 'main' , array( 'menu' => 57 ) );
							$ubermenu_cached->store();
						}
					} else {
						ubermenu( 'main' , array( 'menu' => 57 ) );
					} 
					?>
					<div class="loginout-area-wrapper">
						<div class="loginout-area">
							<ul>
								<li>
									<?php 
									if ( is_user_logged_in() ) : 
										$current_user = wp_get_current_user();

									if ( $current_user->user_firstname && $current_user->user_lastname ) {
										$show_name = $current_user->first_name . ' ' . $current_user->last_name;
									} elseif ( $current_user->user_firstname ) {
										$show_name = $current_user->first_name;
									} else {
										$show_name = $current_user->user_login;
									}
									?>
									<span class="item_welcome"><a href="/account"><?php echo $show_name; ?></a></span> | <span class="item_logout"><a href="<?php echo wp_logout_url( get_permalink() ); ?>">Logout</a></span>
								<?php else : ?>
									<span class="item_login"><a href="/account">Login</a></span> | <span class="item_register"><a href="/subscribe">Subscribe</a></span>
								<?php endif; ?>
							</li>
						</ul>
					</div>
				</div>
				<?php
				global $global_title;
				if(is_category()){
					$global_title = single_cat_title('',false);
				}elseif(is_tag()){
					$global_title = single_tag_title('',false);
				}elseif(is_tax()){
					$global_title = single_term_title('',false);
				}elseif(is_author()){
					$global_title = __("Author: ",'cactusthemes') . get_the_author();
				}elseif(is_day()){
					$global_title = __("Archives for ",'cactusthemes') . date_i18n(get_option('date_format') ,strtotime(get_the_date()));
				}elseif(is_month()){
					$global_title = __("Archives for ",'cactusthemes') . get_the_date('F, Y');
				}elseif(is_year()){
					$global_title = __("Archives for ",'cactusthemes') . get_the_date('Y');
				}elseif(is_home()){
					if(get_option('page_for_posts')){ $global_title = get_the_title(get_option('page_for_posts'));
				}else{
					$global_title = get_bloginfo('name');
				}
			}elseif(is_404()){
				$global_title = '404!';
			}else{
				global $post;
				if($post)
					$global_title = $post->post_title;
			}
		get_template_part( 'header', 'navigation' ); // load header-navigation.php 
		
		if(is_single()  && !is_attachment()){
			if(tm_is_post_video()){
				$get_layout = get_post_meta($post->ID,'page_layout',true);
				if($get_layout=='def' || $get_layout==''){$get_layout = ot_get_option('single_layout_video');}
				if($get_layout!='inbox'){
					get_template_part( 'header', 'single-player' );
				}
			}else{
				get_template_part( 'header', 'single' );
			}
		}elseif(is_category()&&!is_search()){
			get_template_part( 'header', 'category' );
		}elseif(is_front_page()||is_page_template('page-templates/front-page.php')){
			get_template_part( 'header', 'frontpage' );
		}elseif(is_plugin_active('buddypress/bp-loader.php') && bp_is_current_component('playlist')){
			get_template_part( 'header', 'playlist' );
		}
		global $sidebar_width;
		$sidebar_width = ot_get_option('sidebar_width');
		?>
		<?php
		if($blog_show_meta_grid2 = ot_get_option('blog_show_meta_grid2') && !is_category()){ ?>
		<style>
			.video-listing.style-grid-2 .item-content.hidden,
			.video-listing.style-grid-2 .item-info.hidden{
				display: block !important;
				visibility: visible !important;
			}
		</style>
		<?php
	}
	if($cat_show_meta_grid2 = ot_get_option('cat_show_meta_grid2') && is_category() ){ ?>
	<style>
		.video-listing.style-grid-2 .item-content.hidden,
		.video-listing.style-grid-2 .item-info.hidden{
			display: block !important;
			visibility: visible !important;
		}
	</style>
	<?php
}?>

</header>

