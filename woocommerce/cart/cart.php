<?php
/**
 * Cart Page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<form action="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" method="post">

	<?php do_action( 'woocommerce_before_cart_table' ); ?>

	<table class="shop_table cart" cellspacing="0">
		<thead>
			<tr>
				<th class="product-remove">&nbsp;</th>
				<th class="product-thumbnail">&nbsp;</th>
				<th class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
				<th class="product-price"><?php _e( 'Price', 'woocommerce' ); ?></th>
				<th class="product-quantity"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
				<th class="product-subtotal"><?php _e( 'Total', 'woocommerce' ); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

			$featured_webcast_prods = array( 50118,	// Millions On The Phone sold by CTTI
					56172,	// Millions On The Phone for Addicted To Success affiliate
					58265	// The 10X Rule Webcast
					);

			if ( in_array( $product_id,  $featured_webcast_prods ) )
				$is_featured_webcast = true;

			// Check if is Millions On The Phone sold by CTTI
			if ( $product_id == 50118 )
				$motp_lessons = array( 'How to Handle Price on the Phone',
					'How to Fill Your Pipeline',
					'What to Never Say on the Phone',
					'How to Create Urgency',
					'And more...' );

			// Check if is Millions On The Phone for Addicted To Success affiliate
			if ( $product_id == 56172 ) {
				$is_motp_ats = true;
				$motp_lessons = array( 'How to Handle Price on the Phone',
					'How to Fill Your Pipeline',
					'What to Never Say on the Phone',
					'How to Create Urgency',
					'And more...' );
			}

			// Check if is The 10X Rule Webcast
			if ( $product_id == 58265 )
				$tenx_rule_lessons = array( '10X Your Income',
					'Exceed Your Targets',
					'Become a Multi-Millionaire',
					'Guarantee Your Success',
					'Find Your Purpose',
					'Create 10X Wealth' );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				?>
				<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

					<td class="product-remove">
						<?php
						echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="remove" title="%s">&times;</a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'woocommerce' ) ), $cart_item_key );
						?>
					</td>

					<td class="product-thumbnail <?php if($is_featured_webcast): ?>featured-webcast-cart-image<?php endif ?>">
						<?php
						$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

						if ( ! $_product->is_visible() )
							echo $thumbnail;
						else
							printf( '<a href="%s">%s</a>', $_product->get_permalink( $cart_item ), $thumbnail );
						?>
					</td>

					<td class="product-name">
						<?php
						if ( ! $_product->is_visible() )
							echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
						else
							echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', $_product->get_permalink( $cart_item ), $_product->get_title() ), $cart_item, $cart_item_key );

							// Meta data
						echo WC()->cart->get_item_data( $cart_item );

               				// Backorder notification
						if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) )
							echo '<p class="backorder_notification">' . __( 'Available on backorder', 'woocommerce' ) . '</p>';
						?>
					</td>

					<td class="product-price">
						<?php
						echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
						?>
					</td>

					<td class="product-quantity">
						<?php
						if ( $_product->is_sold_individually() ) {
							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
						} else {
							$product_quantity = woocommerce_quantity_input( array(
								'input_name'  => "cart[{$cart_item_key}][qty]",
								'input_value' => $cart_item['quantity'],
								'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
								'min_value'   => '0'
								), $_product, false );
						}

						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key );
						?>
					</td>

					<td class="product-subtotal">
						<?php
						echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
						?>
					</td>
				</tr>
				<?php
			}
		}

		do_action( 'woocommerce_cart_contents' );
		?>
		<tr>
			<td colspan="6" class="actions">

				<?php if ( WC()->cart->coupons_enabled() ) { ?>
				<?php if (!$is_motp_ats): ?>
					<div class="coupon">

						<label for="coupon_code"><?php _e( 'Coupon', 'woocommerce' ); ?>:</label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php _e( 'Coupon code', 'woocommerce' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php _e( 'Apply Coupon', 'woocommerce' ); ?>" />

						<?php do_action( 'woocommerce_cart_coupon' ); ?>

					</div>
				<?php endif ?>
				<?php } ?>

				<input type="submit" class="button" name="update_cart" value="<?php _e( 'Update Cart', 'woocommerce' ); ?>" />

				<?php do_action( 'woocommerce_cart_actions' ); ?>

				<?php wp_nonce_field( 'woocommerce-cart' ); ?>
			</td>
		</tr>

		<?php do_action( 'woocommerce_after_cart_contents' ); ?>
	</tbody>
</table>

<?php do_action( 'woocommerce_after_cart_table' ); ?>

</form>

<div class="cart-collaterals">

	<?php if ($is_featured_webcast): ?>

		<?php
		// Check if there are more than one featured webcast in the cart
		if($tenx_rule_lessons && $motp_lessons)
			$multiple_webcasts = true;
		?>

		<div class="featured-webcast-cart-desc">
			<p>In this webcast<?php if($multiple_webcasts): ?>s<?php endif ?> Grant Cardone will teach you:</p>
			<ul>
				<?php if ($tenx_rule_lessons): ?>

					<?php if($multiple_webcasts): ?>
						<strong>The 10X Rule Webcast</strong>
					<?php endif ?>

					<? foreach ($tenx_rule_lessons as $lesson): ?>
					<li><?= $lesson ?></li>
				<? endforeach; ?>

			<?php endif ?>
			
			<?php if ($motp_lessons): ?>

				<?php if($multiple_webcasts): ?>
					<br>
					<strong>Millions On The Phone Webcast</strong>
				<?php endif ?>

				<? foreach ($motp_lessons as $lesson): ?>
				<li><?= $lesson ?></li>
			<? endforeach; ?>

		<?php endif ?>
	</ul>
</div>

<?php endif ?>

<?php do_action( 'woocommerce_cart_collaterals' ); ?>

</div>

<?php if(!$is_featured_webcast){ do_action( 'woocommerce_after_cart' ); } ?>
