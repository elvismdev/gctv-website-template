<?php
/**
 * Thankyou page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


if ( $order ) : ?>

<?php if ( $order->has_status( 'failed' ) ) : ?>




	<p><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction.', 'woocommerce' ); ?></p>

	<p><?php
		if ( is_user_logged_in() )
			_e( 'Please attempt your purchase again or go to your account page.', 'woocommerce' );
		else
			_e( 'Please attempt your purchase again.', 'woocommerce' );
		?></p>

		<p>
			<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
			<?php if ( is_user_logged_in() ) : ?>
				<a href="<?php echo esc_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ); ?>" class="button pay"><?php _e( 'My Account', 'woocommerce' ); ?></a>
			<?php endif; ?>
		</p>

	<?php else : ?>

		<?php
		$millionsonthephone_prods = array(50118, // Millions On The Phone sold by CTTI
					56172 // Millions On The Phone for Addicted To Success affiliate
					); 
		$items = $order->get_items();
		$productsIds = array();
		foreach ( $items as $item ) {
			$productsIds[] = $item['product_id'];
			if (in_array($item['product_id'],  $millionsonthephone_prods)) {
				$millionsonthephone = true;
			}
		}

		$webinars = array(6265, 6865, 7895, 9789, 9888, 50118, 56172);
		?>

		<?php if (!array_intersect($webinars, $productsIds)) { ?>
		<p><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); ?></p>
		<p style="font-size: 14px;">
			<em>If your order contains a virtual product (e.g. MP3 or Zip files) please make sure you follow <a href="http://grantcardonetv.com/download-instructions">our recommended instructions to download your products.</a></em>
		</p>
		<?php } else { ?>

		<?php if (!$millionsonthephone): ?>
			<h1><?php _e( 'Congratulations for making the choice to invest in yourself and your future by signing-up for one of our on-demand events!', 'woocommerce' ); ?></h1>
		<?php else: ?>
			<h1><?php _e( 'Congratulations for signing up for Millions On The Phone Video Webcast - GC', 'woocommerce' ); ?></h1>
			<p>Hey, you really must be addicted to success, hooking up with us for my Millions on the Phone webcast! 
			Congratulations on making the best investment—yourself! 
			Most people only work enough to not get fired, whereas successful people work at a pace that gets such satisfying results that work is a reward. 
			Truly successful people don’t even call it 'work'; for them, it’s passion. 
			There are almost one trillion phone calls made every year just in the USA.
			Make sure yours are getting you closer to your dreams.</p>

			<p>Since the webcast, we have already had hundreds of people write in with successes from using the 86-page eBook to make sales. Here are a few:</p>

				<p><em>“Closed a $60,000 deal 2 days after listening to Grant Cardone on Millions on the Phone. All it took was two calls!” - Jonathan S. Nani (<a href="https://twitter.com/JonathanNani" target="_blank">@JonathanNani</a>)</em></p>

				<p><em>“Just now getting through and grasping the info put out on the MOTP webcast. I am taking tomorrow to design my whole Monday, week, and year around it. Best $50 I have ever spent. Blown away is putting it mildly.” - Greg McIntyre (<a href="https://twitter.com/LawyerGreg" target="_blank">@LawyerGreg</a>)</em></p>

				<p><em>“Thursday night. Third time watching the webinar. Thank you.” - Frank Enriquez (<a href="https://twitter.com/nrekez" target="_blank">@nrekez</a>)</em></p>

				<p><em>“That knocked my shoes off! Damn!” - Javier Navarro (<a href="https://twitter.com/nrekez" target="_blank">@jnavarrof</a>)</em></p>
				<p>SHARE your experience!</p>
				<ul class="list-inline checkout-share">
					<li>
						<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.millionsonthephone.com/" data-text="I just sign up for #MillionsOnThePhone with @GrantCardone" data-via="GrantCardoneTV" data-count="none">Tweet</a>
					</li>
					<li>
						<div class="fb-share-button" data-href="http://www.millionsonthephone.com/" data-layout="button"></div>
					</li>
					<li>
						<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
						<script type="IN/Share" data-url="http://www.millionsonthephone.com/"></script>
					</li>
					<li>
						<!-- Place this tag in your head or just before your close body tag. -->
						<script src="https://apis.google.com/js/platform.js" async defer></script>

						<!-- Place this tag where you want the share button to render. -->
						<div class="g-plus" data-action="share" data-annotation="none" data-href="http://www.millionsonthephone.com/"></div>
					</li>
				</ul>

				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=724977984178978";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
			<?php endif ?>

			<?php } ?>

			<br /><br />
			<ul class="order_details">
				<li class="order">
					<?php _e( 'Order:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_order_number(); ?></strong>
				</li>
				<li class="date">
					<?php _e( 'Date:', 'woocommerce' ); ?>
					<strong><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></strong>
				</li>
				<li class="total">
					<?php _e( 'Total:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_formatted_order_total(); ?></strong>
				</li>
				<?php if ( $order->payment_method_title ) : ?>
					<li class="method">
						<?php _e( 'Payment method:', 'woocommerce' ); ?>
						<strong><?php echo $order->payment_method_title; ?></strong>
					</li>
				<?php endif; ?>
			</ul>
			<div class="clear"></div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_thankyou_' . $order->payment_method, $order->id ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->id ); ?>

		<?php 
		if (empty($_GET['paypal'])) :
			?>

<?php endif; ?>
<?php else : ?>
	<p><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>
<?php endif; ?>