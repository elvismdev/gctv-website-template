<?php
/**
 * Customer completed order email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php $items = $order->get_items(); ?>

<?php
$millionsonthephone_prods = array(50118, // Millions On The Phone sold by CTTI
					56172 // Millions On The Phone for Addicted To Success affiliate
					);

foreach ( $items as $item ) {
	if ( in_array( $item['product_id'],  $millionsonthephone_prods ) ) {
		$millionsonthephone = true;
	}
	if ( $item['product_id'] == 58265 ) {
		$the_10x_rule_webcast = true;
	}
}
?>

<?php do_action( 'woocommerce_email_header', $email_heading ); ?>


<?php if ($millionsonthephone) { // Check if is Millions On The Phone ?>

<p>Thank you for registering for our How to Make Millions on the Phone video webcast!</p>

<p>Below are instructions on how to access the event:</p>

<ol>
	<li><a href="http://grantcardonetv.com/millionsonthephone/webcast/"><strong>Click here to access the webcast</strong></a></li>
	<li>Use your GCTV username and password to log in (please use the one you registered for this event with)</li>
</ol>

<?php } elseif ($the_10x_rule_webcast) { // Check if is The 10x Rule Webcast ?>

<p><a href="http://grantcardonetv.com/10xsuperlife/10x-super-life/" target="_blank"><b>CLICK HERE TO WATCH THE WEBCAST</b></a></p>

<p>Thank you for registering to Grant Cardone’s 10X Super Life video webcast!</p>

<p>Here is what you need to know:</p>

<ol>
	<li>Use your GCTV username and password to log in (please use the one you registered for this event with)</li>
	<li>The webcast will be available to replay 24/7 at <a href="http://grantcardonetv.com/10xsuperlife/10x-super-life/">grantcardonetv.com</a>.</li>
</ol>

<p>Thank you again for registering. Keep an eye out for our emails and make sure your friends and colleagues know about this amazing opportunity!</p>

<?php } else { ?>

<p><?php printf( __( "Thank you for investing in yourself, and your future!. Your recent purchase on %s has been completed. Your order details are shown below for your reference:", 'woocommerce' ), get_option( 'blogname' ) ); ?></p>

<p>
	<strong>
		<em>If your order contains a virtual product (e.g. MP3 or Zip files) please make sure you follow <a href="http://grantcardonetv.com/download-instructions">our recommended instructions to download your products.</a></em>
	</strong>
</p>

<?php } ?>

<?php do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text ); ?>

<h2><?php echo __( 'Order:', 'woocommerce' ) . ' ' . $order->get_order_number(); ?></h2>

<table cellspacing="0" cellpadding="6" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
	<thead>
		<tr>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e( 'Price', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo $order->email_order_items_table( true, false, true ); ?>
	</tbody>
	<tfoot>
		<?php
		if ( $totals = $order->get_order_item_totals() ) {
			$i = 0;
			foreach ( $totals as $total ) {
				$i++;
				?><tr>
				<th scope="row" colspan="2" style="text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
				<td style="text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['value']; ?></td>
			</tr><?php
		}
	}
	?>
</tfoot>
</table>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text ); ?>

<h2><?php _e( 'Customer details', 'woocommerce' ); ?></h2>

<?php if ($order->billing_email) : ?>
	<p><strong><?php _e( 'Email:', 'woocommerce' ); ?></strong> <?php echo $order->billing_email; ?></p>
<?php endif; ?>
<?php if ($order->billing_phone) : ?>
	<p><strong><?php _e( 'Tel:', 'woocommerce' ); ?></strong> <?php echo $order->billing_phone; ?></p>
<?php endif; ?>

<?php wc_get_template( 'emails/email-addresses.php', array( 'order' => $order ) ); ?>

<?php do_action( 'woocommerce_email_footer' ); ?>