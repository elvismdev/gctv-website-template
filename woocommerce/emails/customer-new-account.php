<?php
/**
 * Customer new account email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// load customer data and user email into email template
$user_email = $user_login;
$user       = get_user_by('login', $user_login);

if ( $user )
	$user_email = $user->user_email;

if ( !$user_pass )
	$user_pass = sanitize_text_field( ( isset( $_POST['account_password'] ) ? $_POST['account_password'] : '' ) );

?>

<?php do_action( 'woocommerce_email_header', $email_heading ); ?>

<p><?php printf( __( "Thank you for creating an account on %s. Below is your login information used to access paid and premium content.<br><br>
	Username: <strong>%s</strong><br>
	Password: <strong>%s</strong>", 
	'woocommerce' ), esc_html( $blogname ), esc_html( $user_email ), esc_html( $user_pass ) ); ?></p>

	<?php if ( get_option( 'woocommerce_registration_generate_password' ) == 'yes' && $password_generated ) : ?>

		<p><?php printf( __( "Your password has been automatically generated: <strong>%s</strong>", 'woocommerce' ), esc_html( $user_pass ) ); ?></p>

	<?php endif; ?>

	<p><?php printf( __( 'You can login and access your account area by going to this link: %s.', 'woocommerce' ), wc_get_page_permalink( 'myaccount' ) ); ?></p>

	<?php do_action( 'woocommerce_email_footer' ); ?>
