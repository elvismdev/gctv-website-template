<?php
/**
 * Sidebar
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
echo '<div id="shop-sidebar" class="col-md-3">';
dynamic_sidebar( 'Shop' );
echo '</div>';
?>
<div class="clearfix"></div>