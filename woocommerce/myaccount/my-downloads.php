<?php
/**
 * My Orders
 *
 * Shows recent orders on the account page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$purchased_motp = false;
require_once( ABSPATH . 'wp-includes/pluggable.php' );
if ( $group = Groups_Group::read_by_name( 'Millions On The Phone' ) ) {
	$purchased_motp = Groups_User_Group::read( get_current_user_id() , $group->group_id );
}


if ( $downloads = WC()->customer->get_downloadable_products() ) : ?>


<?php do_action( 'woocommerce_before_available_downloads' ); ?>

<h2><?php echo apply_filters( 'woocommerce_my_account_my_downloads_title', __( 'Available downloads', 'woocommerce' ) ); ?></h2>

<ul class="digital-downloads">
	<?php foreach ( $downloads as $download ) : ?>
		<li>
			<?php
			do_action( 'woocommerce_available_download_start', $download );

			if ( is_numeric( $download['downloads_remaining'] ) )
				echo apply_filters( 'woocommerce_available_download_count', '<span class="count">' . sprintf( _n( '%s download remaining', '%s downloads remaining', $download['downloads_remaining'], 'woocommerce' ), $download['downloads_remaining'] ) . '</span> ', $download );

			echo apply_filters( 'woocommerce_available_download_link', '<a href="' . esc_url( $download['download_url'] ) . '">' . $download['download_name'] . '</a>', $download );

			do_action( 'woocommerce_available_download_end', $download );
			?>
		</li>
	<?php endforeach; ?>
</ul>

<?php do_action( 'woocommerce_after_available_downloads' ); ?>

<?php elseif($purchased_motp): ?>

	<?php do_action( 'woocommerce_before_available_downloads' ); ?>

	<h2><?php echo apply_filters( 'woocommerce_my_account_my_downloads_title', __( 'Available downloads', 'woocommerce' ) ); ?></h2>

	<ul class="digital-downloads">
		<li>
			<a target="_blank" href="http://bit.ly/1ESLtdz">How To Make Millions On The Phone eBook – How To Make Millions On The Phone eBook</a>
		</li>
		<li>
			<a target="_blank" href="http://bit.ly/1GbgR7w">Sell or Be Sold [Video Ebook] – SELL OR BE SOLD QR EBOOK</a>
		</li>
	</ul>

	<?php do_action( 'woocommerce_after_available_downloads' ); ?>

<?php endif; ?>



